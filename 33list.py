#-------------list-operation--------------
print(10*"-","list-operation","-"*10)
print()
list1=['Delhi','Panipat','Sonipat','Ambala','Amritsar']
list2=list1
print(list2)

#--------appending new element-----------
print("-"*10,"appendding new element",10*"-")
print()
data=input("Enter new city name :")
list1.append(data)
print(list1)
print()

#--------inserting item at some index-----------
print("-"*10,"inserting item at 4th index","-"*10)
print()
x=input("Enter the data to be inserted :")
list1.insert(4,x)
print()
#----------sorting the list------------------

print("-"*10,"sorting the list","-"*10)
print()
list1.sort()
print(list1)
print()

#----------sorting in decending order-----------
print("-"*10,"sorting the list in decending order","-"*10)
print()
list1.sort(reverse=True)
print(list1)
print()
#------------delete last three elements using pop-operation--------------
print("-"*10,"delete last three elements using pop operation","-"*10)
print()
for i in range(0,3):
    print("Popped Element : ",list1.pop())