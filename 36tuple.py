#--------Built_in Function----------
print(10*"-","Built_in Function",10*"-")
print()

tup1=(2,5,6,9,8,7,4,5,1,2,36,9,8,4)
tup2=(6,5,8,7,4,5,1,333,6,99,9,8,744,2,21,2,454)
print(tup1)
print(tup2)

print("-"*10," Len() ",10*"-")
print()
print("len(tup1) :",len(tup1))
print("len(tup2) :",len(tup2))

print(10*"-"," * Max() * Min() *",10*"-")
print()
print("max(tup1) :",max(tup1))
print("max(tup2) :",max(tup2))
print()
print("min(tup1) :",min(tup1))
print("min(tup2) :",min(tup2))
print()
print(10*"-"," typecasting ",10*"-")
l=["Delhi","Kolkata","Python",209,"Ashok","Shiva"]
print(l)
print("Tuple of given List (tuple()) :",tuple(l))
