#---------------list-----------
print(10*"-","List Operations",10*"-")
print()

list1=['Delhi','Panipat','Sonipat','Ambala','Amritsar']
list2=['Patna','Darbhanga','Muzaffarpur','Ranchi','Hyderabad']
list3=['Kolkata','Haldia','Banglore','Mumbai','Kanpur']

print("-"*10,"city name and Length",10*"-")
print()
for i in list1:
    print("Name : {} , Length : {} ".format(i,len(i)))
print()
for i in list2:
    print("Name : {} , Length : {} ".format(i,len(i)))
print()
for i in list3:
    print("Name : {} , Length : {} ".format(i,len(i)))
    
print()    

print(10*"-","Maximum and Minimum in list",10*"-")
print()
def min_max(list):
    max_1=len(list[0])
    min_1=len(list[0])
    for i in range(0,len(list)):
        if max_1<=len(list[i]):
            max_1=len(list[i])
            data=i
        if min_1>=len(list[i]):
            min_1=len((list[i]))
            p=i
    print("Max length {} city is {}".format(max_1,list[data]))
    print("Min Length {} city is {}".format(min_1,list[p]))

min_max(list1)
print()
min_max(list2)
print()
min_max(list3)
print()

print(10*"-","compare list",10*"-")
print()
list4=list1+list2+list3
min_max(list4)


print()

print(10*"-","Deleting list Elements",10*"-")
print()
def delete(list):
    del list[0:1]
    del list[-1:-2:-1]
    print(list)
print("Before :",list1)
delete(list1)
print()
print("Before :",list2)
delete(list2)
print()
print("Before :",list3)
delete(list3)