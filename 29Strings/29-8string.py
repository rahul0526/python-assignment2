#-----------string methods-----------------
print("-"*10,"string methods","-"*10)
print()

#-------------startswith--------------------
print("-"*10,"startswith",10*"-")
print()
x=input("Enter some string :")
print(x)
print("startswith :",x.startswith("Hello"))#Returns True if the string starts with the specified  value
print()

#--------------strip------------------------
print("-"*10,"strip",10*"-")
print()
x=input("Enter some string :")
print("Before length :",len(x))
d=x.strip()                    #Returns a trimmed version of string
print("Strip :",d)
print("After length :",len(d))
print()

#------------swapcase--------------------
print("-"*10,"swapcase",10*"-")
print()
x=input("Enter some string :")
print("swapcase :",x.swapcase()) #swap cases,lower becomes upper and vice versa
print()

#----------------title-------------------
print("-"*10,"title",10*"-")
x=input("Enter some string :")
print("Title :",x.title()) #Converts the first character of each word to upper case
print()

#-----------upper------------------------
print("-"*10,"upper",10*"-")
print()
print(x)
print("Upper Case :",x.upper())



