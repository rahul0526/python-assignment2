#---------string methods----------------
print(10*"-","string Methods","-"*10)
print()
x=input("Enter some string :")

#--------------------partition-------------
print(10*"-","Partition","-"*10)
txt="Python is difficult !"
print(txt)
print("Partition :",txt.partition('difficult')) #Returns a tuple where the string is parted into three parts
print()

#--------------------replace----------------
print(10*"-","replace","-"*10)
txt="Python is difficult"
print(txt)
print("Replace :",txt.replace("difficult","easy"))
print()

#-----------rfind()---------------------
print(10*"-","rfind",10*"-")
print("Rfind :",txt.rfind("is")) #searches the string for a specified value and returns the last position of where it was found
print()

#-------------rindex--------------------
print(10*"-","rindex","-"*10)
print("Rindex :",txt.rindex("Python"))#searches the string for a specified value and returns the last position of where it was found and returns error if not found
print()

#-----------rjust-----------------------
print(10*"-","rjust",10*"-")
print("Rjust :",txt.rjust(30))
print()
