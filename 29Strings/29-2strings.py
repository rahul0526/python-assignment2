#-------String function -----------
print(10*"-","String function",10*"-")
x=input("Enter some string :")
y=input("Enter some substring :")

#----------------------------------
print(10*"-","Ends with",10*"-")

print("Endswith () :",x.endswith(y)) #Returns true if the string ends with the specified value

#------------------------------
print(10*"-","Find",10*"-")

print("Find () :",x.find(y)) #searches he string for a specified value and returns the position of where it was found
print()

#-----------------------------
print(10*"-","ExpandTabs",10*"-")

expand="h\te\tl\tl\to"
tabs=int(input("Enter the specified tabs to set :"))
print("expandtabs ():",expand.expandtabs(tabs)) #sets the tab size of the string
print()


#-----------------------------------
print(10*"-","Format",10*"-")

name=input("Enter your name :")
age=input("Enter your age :")
print(" Using Format specifier \n")
print("Hello {} ! Your age is {} .".format(name,age))#Formats specified value in string

#------------------------------------
print(10*"-","Index",10*"-")#searches the string for a specified valueand returns the positionof whereit was found.else gives error
s=input("Enter some string :")
i=input("Enter the string to search :")
print("Index of {} is {} ".format(i,s.index(i)))