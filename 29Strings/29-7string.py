#--------string opeertaions-----------
print(10*"-","String Operations","-"*10)
print()
txt="Python is awesome !"
print(txt)

#--------rpartition--------------------

print("-"*10,"rpartition",10*"-")
print("rpartition :",txt.rpartition("awesome")) #Returns a tuple where the string is parted into three parts
print()

#------------rsplit--------------
print("-"*10,"rsplit",10*"-")
x=input("Enter some string for Rsplit testing separated by '-':")
print("rsplit :",x.rsplit("-",1))#splits the string at specified separator,and returns the list.takes a max parameter
print()

#----------rstrip------------------
print("-"*10,"rstrip",10*"-")
x=input("Enter some string for rstrip testing :")
print("Length Before :",len(x))
d=x.rstrip() #Returns the right trim version of the string 
print("After Rstrip = {} len = {}".format(d,len(d)))
print()

#-----------split--------------------
print("-"*10,"split",10*"-")
x=input("Enter some string for split separated by space :")
print("Split :",x.split(" "))#Splits the string at the specified separator and returns the list
print()

#---------splitlines------------------
print("-"*10,"splitlines",10*"-")
txt="This\nis\nthe\ninitiaive"
print("txt")
print("splitlines :",txt.splitlines())#split the string at line breaks and returns the list,takes one argument of 'keeplinebreaks'
print()

