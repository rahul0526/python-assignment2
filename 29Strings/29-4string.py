#-------------string methods-------------
print(10*"-","String Methods",10*"-")
print()

x=input("Enter some string :")

#-------------islower--------------------
print()
print(10*"-","Islower",10*"-")
print("Islower :",x.islower())#Returns True if all the characters n the string are lower case
print()

#-------------isnumeric-------------------
print()
print(10*"-","isnumeric",10*"-")
print("Isnumeric :",x.isnumeric()) #Returns True if all the characters in the string are numberic
print()

#------------isprintable()----------------- 
print()
print("-"*10,"isprintable",10*"-")
print("Isprintable :",x.isprintable())#Returns True if all the characters in the string is printable  esle false....newline is  non printable charcter
print()
#--------------isspace()--------------------
print()
print("-"*10,"isspace()","-"*10)
print("Isspace :",x.isspace())#Returns True if all the charcters in the string are whitespaces 
print()

#---------------istitle--------------------
print()
print("-"*10,"istitle",10*"-")
print("Ititle :",x.istitle())#Returns True if the string follows the rules of a Ititle
print()