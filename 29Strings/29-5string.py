#--------string method------------
print("-"*10,"String method",10*"-")
print()

x=input("Enter some string :")
d="Python"
#--------isupper------------------
print()
print(10*"-","isupper",10*"-")
print("isupper :",x.isupper())#Return True if all character n te string are in upper case
print()

#---------join----------------------
print(10*"-","Join","-"*10)
print()
l=[d for d in input("Enter the string :")]
print("Join :","-".join(l))      # Join the elements of an iterable to the end of string
print()

#-------------ljust----------------
print(10*"-","ljust","-"*10)
print("ljust :",d.ljust(10)," is awesome.")#Return a left justified version of the string
print()

#---------------lower---------------
print(10*"-","lower",10*"-")
print("Lower :",x.lower()) #Converts string into lower case
print()

#-----------lstrip----------------
print(10*"-","lstrip","-"*10)
print("Before Lstrip :",x)
print("lstrip :",x.lstrip()) #Returns a left trim version of string
print()
