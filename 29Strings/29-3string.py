#----------string methods----------------------
print(10*"-","String Methods",10*"-")
print()
x=input("Enter some String :")

#-----------isalnum---------------
print()
print(10*"-","isalnum",10*"-")
print("isalnum :",x.isalnum())#Returns True if all the characters in String are alphanumeric
print()

#-----------isalpha---------------
print()
print(10*"-","isalpha",10*"-")
print("isalpha :",x.isalpha())#Returns True if all the characters in the String are in alphabet
print()
#-----------isdecimals-------------
print()
print(10*"-","isdecimals",10*"-")
print("isdecimal :",x.isdecimal())#Returns True if all the characters in the String are decimals
print()

#-----------isidentifier-------------
print()
print(10*"-","Isidentifier","-"*10)
print("Identifier :",x.isidentifier())#Returns True if the string is an Identifier
print()

#-------------isdigit----------------
print()
print(10*"-","isdigit","-"*10)
print("Isdigit :",x.isdigit())#Returns True if all the characters of string is digit
print()