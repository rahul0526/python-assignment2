#---------Built In funtions of String---
print("-"*10,"Built In Function of String",10*"-")
x=input("Enter some string :")
y=input("Enter any substring :")
print("Capitalize :",x.capitalize()) #converts first character to upper case
print("Casefold :",x.casefold()) #Converts string into lowercase
print("Center :",x.center(40)) #Returns the center string 
print("Count :",x.count(y)) #Returns number of times y is present in x
print("Encode :",x.encode()) #Returns the encoded version 