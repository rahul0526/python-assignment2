#---------Vowels In String-------
print("-"*10,"Vowels In String",10*"-")
vowels=['a','e','i','o','u']
x=input("Enter the string to check for vowels :").lower()
d={}
for i in x:
    if i in vowels:
        d[i]=d.get(i,0)+1
print("Number of total Vowels =",sum(d.values()))
for k,v in sorted(d.items()):
    print("{} = {} ".format(k,v),end=" ")
print()