#using the built-in function on Numbers to perform following operations

#--------------Round of the given floating point number.------------------
x=float(input("Enter the floating point number :"))
y=round(x)
print("Value after rounding the number  :",y)
print("\n"*3)

#-----------finding the square root of the number using sqrt function----------------
print("-"*10,"finding the square root of the number using sqrt function",10*"-")
from math import sqrt 
x=eval(input("Enter a digit :"))
p=sqrt(x)
print("Square Root of {} is {}".format(x,p))
print("\n"*3)

#--------Generate random number between 0 and 1-----------
print("-"*10,"Generate random number between 0 and 1",10*"-")
import random
print(random.random())
print("\n"*3)


#--------Generate random number between 10 and 500----------
print("-"*10,"Generate random number between 0 and 1",10*"-")
print(random.uniform(10,500))