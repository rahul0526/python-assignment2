#--------sorting--------------
print("-"*10,"Selection Sorting",10*"-")
s=[int(x) for x in input("Enter the sequence of number separated by ',': ").split(",")]
for i in range(len(s)):
    minm=i
    for j in range(i+1,len(s)): 
        if s[minm]>s[j]:
            minm=j 
    s[i],s[minm]=s[minm],s[i]
print("Sorted List :",end=" ")
for p in s:
    print(p,end=" ")
print()