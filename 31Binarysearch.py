#------------Binary search------------------------
def Binarysearch(l,low,high,item):
    while low<=high:
        mid=low+(high-low)//2
        if item==l[mid]:
            return mid
        elif item>l[mid]:
            low=mid+1
        else:
            high=mid-1 
    return -1

print(10*"-","Binary Search",10*"-")
print()

l=[int (x) for x in input("Enter the sequence of number :")]
l.sort()
low=0 
high=len(l)-1
item=int(input("Enter the item to be searched :"))

result=Binarysearch(l,low,high,item)

if result!=-1:
    print("Search successful. Item present at {} index ".format(result +1))
else:
    print("Unsuccessful Search, Item not present .")




            