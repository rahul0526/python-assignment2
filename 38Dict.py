#---------Dictionary Operations--------
print(10*"-","Dictionary Operations",10*"-")
print()
dict1 ={'Name':'Ramakrishna','Age':25}
dict2={'EmpId':1234,'Salary':5000}

#------------------------------------------
print(30*"-")
print()
dict3=dict1.copy()
dict3.update(dict2)
print("Merged Dictionary :",dict3)
print()

#-------------------------------------------
print(30*"-")
print()
dict3['Salary']+=dict3["Salary"]*10//100
print(dict3)
print()
#----------------------------------------------
dict3["Age"]=26

#------------------------------
dict3["Grade"]="B1"

print("Updated Dictionary :")
for k,v in dict3.items():
    print("{}  ----  {}".format(k,v))
print()
#---------------------------------
print(30*"-")
dict3.pop("Age")
print("Dictionary after removing AGE :")
print(dict3)