#-----------Escape Characters---------------
print(10*"-","Escape Characters",10*"-")
print()

print(5*"-","Backslash",5*"-")
print("Path :", "~\\home\\Desktop\\Python3\\")
print()

print(5*"-","Single Quote",5*"-")
print("what\'s the matter... Python is very easy.")
print()

print(5*"-","Double Quote",5*"-")
print('You should use "Double Quote" when needed.')
print()

print(5*"-","Horizontal Tab",5*"-")
print("Hello \t world")
print()

print(5*"-","Carriage Return",5*"-")
print("Hello \rworld!")
print()

print(5*"-","Vertical Tab",5*"-")
print("Hello \v World!")
print()

print(5*"-","New Line",5*"-")
print("This is Optimus Prime \nWe need to Change for good.")
print()

print(5*"-","Formfeed",5*"-")
print("Hello \f world")
print()

print(5*"-","Backspace",5*"-")
print("ab"+"\b"+"c")
print()
