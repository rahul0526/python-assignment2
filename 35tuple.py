#---------tuple--------------
print("-"*10,"tuple operations",10*"-")
print()

tup1=("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
for x in tup1:
    print(x)
print()
#---------concatination--------
print(30*"-")
tup2=("January","February","March","April","May","June","July","August","September","October","November","December")
tup3=tup1+tup2
print("Elements after concatination :")
for i in tup3:
    print(i)
print()

#-------------greater-------------
print(30*"-")
tup4=tuple((x for x in range(0,10)))
tup5=tuple((x for x in range(10,20)))
tup6=tuple((x for x in range(20,30)))
tup7=tup4+tup5+tup6
print("Greatest Number :{}".format(max(tup7)))

#----------deleting individual element in tuple------
#print(30*"-")


#-----------inserting new element into tuple-----------
print(30*"-")
print()
l=list(tup1)
x=input("Enter any element for insertion :")
l.append(x)
print(l)