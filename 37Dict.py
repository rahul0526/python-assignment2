#-----------Dictionary -----------------
print(10*"-","Dictionary",10*"-")
print()
dict1={100:'A',200:'B',300:'C'}
dict2={400:'D',500:'E',600:'F'}
dict3={700:'G',800:'H',900:'I'}
#-----------comparing the Dictionary----------------
print(10*"-","Comparing the Dictionary",10*"-")
print()
biggest=str(dict1) if str(dict1)>str(dict2)>str(dict3) else str(dict2) if str(dict2)>str(dict3) else str(dict3)
print("Biggest is :",biggest)
print()
#---------Adding new elemement in Dictionaries------
x=input("Enter the key for dict2 :")
y=input("Enter the respective data for dict2 :")
dict2[x]=y
print(dict2)
print()
x=input("Enter the key for dict3 :")
y=input("Enter the respective data for dict3 :")
dict3[x]=y
print()
#----------length---------------------------------
print("-"*10,"Length",10*"-")
print()
print("Length of dict1 :",len(dict1))
print()
print("Length of dict2 :",len(dict2))
print()
print("Length of dict3 :",len(dict3))
print()

#---------converting Dictionary into string and concatinating them---------
print(10*"-","Converting Dictionary into string",10*"-")
print()
str1=str(dict1)
str2=str(dict2)
str3=str(dict3)
str1=str1+str2+str3
print(str1)